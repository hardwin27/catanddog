import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    padding: 50
  },
  title: {
      textAlign: 'center',
      fontSize: 50,
      marginVertical: 50
  },
  text: {
      fontSize: 20
  }
});

export default styles;