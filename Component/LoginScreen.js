import React, {Component} from 'react';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity
} from 'react-native';
import {connect} from 'react-redux';

import Style from '../assets/Style/Styles.js';

class Login extends Component {
    render() {
        const {navigation} = this.props;
        return(
            <View style={Style.container}>
                <Text style={Style.title}>Random Cat and Dog</Text>
                <Text style={Style.text}>For your dose of cute cat and dog</Text>
                <View style={{marginVertical: 50}}>
                    <TextInput
                        placeholder="What's your name?"
                        defaultValue=""
                        onChangeText={text => this.props.changeName(text)}
                    />
                    <TouchableOpacity
                        onPress={() => navigation.push('Home')}
                    >
                        <Text style={Style.text}>Confirm</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

function stateToProps(state) {
    return {
        name:state.name
    };
}

function dispatchToProps(dispatch) {
    return {
        changeName: (newName) => dispatch({type: 'CHANGE_NAME', name: newName })
    }
}

export default connect(stateToProps, dispatchToProps)(Login);