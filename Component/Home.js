import React, {Component} from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import {connect} from 'react-redux';

import Style from '../assets/Style/Styles.js';

class Home extends Component {
    render() {
        const {navigation} = this.props;
        return(
            <View style={Style.container}>
                <Text style={Style.text}>Hello, {this.props.name}</Text>
                <Text style={Style.text}>What do you want to see?</Text>
                <View style={{flexDirection: 'row', marginVertical: 20}}>
                    <TouchableOpacity
                        onPress={() => navigation.push('CatScreen')}
                    >
                        <Text style={Style.text}>Cat</Text>
                    </TouchableOpacity>
                    <Text style={Style.text}> or </Text>
                    <TouchableOpacity
                        onPress={() => navigation.push('DogScreen')}
                    >
                        <Text style={Style.text}>Dog</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity>
                    <Text Style={Style.text}>About Us</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

function stateToProps(state) {
    return {
        name: state.name
    };
}

function dispatchToProps(dispatch) {
    return {
        // updateCats: (cats) => dispatch({type: 'UPDATE_CATS', cats: cats }),
        // updateDogs: (dogs) => dispatch({type: 'UPDATE_DOGS', dogs: dogs })
    }
}

export default connect(stateToProps, dispatchToProps)(Home);