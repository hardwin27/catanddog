import React, {Component} from 'react';
import {
    View,
    FlatList,
    Image
} from 'react-native';
import {connect} from 'react-redux';

import Style from '../assets/Style/Styles.js';

class Dog extends Component {
    getDogs(n, dogs) {
        this.props.updateDogs([])
        return fetch('https://random.dog/woof.json')
        .then((response) => response.json())
        .then((json) => {
            dogs.push(json.url);
        })
        .then(() => {
            n--;
            if(n > 0) {
                this.getDogs(n, dogs)
            }
            else {
                this.props.updateDogs(dogs);
            }
        })
    }
    componentDidMount() {
        this.getDogs(10, []);
    }
    render() {
        return(
            <View style={Style.container}>
                <FlatList
                    data={this.props.dogImages}
                    renderItem={(data) => {
                        return (
                        <View style={{margin: 50}}>
                            <Image
                                source={{uri: data.item}}
                                style={{width: 300, height: 300}}
                                resizeMode='contain'
                            />
                        </View>
                    )}}
                />
            </View>
        )
    }
}


function stateToProps(state) {
    return {
        dogImages: state.dogImages
    };
}

function dispatchToProps(dispatch) {
    return {
        updateDogs: (dogs) => dispatch({type: 'UPDATE_DOGS', dogs: dogs })
    }
}

export default connect(stateToProps, dispatchToProps)(Dog);