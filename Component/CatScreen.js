import React, {Component} from 'react';
import {
    View,
    FlatList,
    Image
} from 'react-native';
import {connect} from 'react-redux';

import Style from '../assets/Style/Styles.js';

class Cat extends Component {
    getCats(n, cats) {
        this.props.updateCats([])
        return fetch('https://aws.random.cat/meow')
        .then((response) => response.json())
        .then((json) => {
            cats.push(json.file);
        })
        .then(() => {
            n--;
            if(n > 0) {
                this.getCats(n, cats)
            }
            else {
                this.props.updateCats(cats);
            }
        })
    }
    componentDidMount() {
        this.getCats(10, []);
    }
    render() {
        return(
            <View style={Style.container}>
                <FlatList
                    data={this.props.catImages}
                    renderItem={(data) => {
                        return (
                        <View style={{margin: 50}}>
                            <Image
                                source={{uri: data.item}}
                                style={{width: 300, height: 300}}
                                resizeMode='contain'
                            />
                        </View>
                    )}}
                />
            </View>
        )
    }
}

function stateToProps(state) {
    return {
        name: state.name,
        catImages: state.catImages
    };
}

function dispatchToProps(dispatch) {
    return {
        updateCats: (cats) => dispatch({type: 'UPDATE_CATS', cats: cats })
    }
}

export default connect(stateToProps, dispatchToProps)(Cat);