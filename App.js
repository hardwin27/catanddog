import React, {Component} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {createStore} from 'redux';
import {Provider} from 'react-redux';

import Login from './Component/LoginScreen.js';
import Home from './Component/Home.js';
import CatScreen from './Component/CatScreen.js';
import DogScreen from './Component/DogScreen.js';

const intiState={
    name: "",
    catImages: [],
    dogImages: []
};

const reducer = (state = intiState, action) => {
    switch(action.type) {
        case 'CHANGE_NAME':
            return {name: action.name};
            break;
        case 'UPDATE_CATS':
            return {catImages: action.cats};
            break;
        case 'UPDATE_DOGS':
            return {dogImages: action.dogs};
            break;
    }
    return state
};

const store = createStore(reducer);

const Stack = createStackNavigator();

class App extends Component {
    render() {
        return(
            <Provider store={store}>
                <NavigationContainer>
                    <Stack.Navigator>
                        <Stack.Screen 
                            name="Login" 
                            component={Login}
                            options={{
                                headerShown: false,
                            }}
                        />
                        <Stack.Screen
                            name="Home"
                            component={Home}
                            options={{
                                headerLeft: null,
                                title: "MEOW AND GUKGUK"
                            }}
                        />
                        <Stack.Screen
                            name="CatScreen"
                            component={CatScreen}
                            options= {{
                                title: "Cat"
                            }}
                        />
                        <Stack.Screen
                            name="DogScreen"
                            component={DogScreen}
                            options= {{
                                title: "Dog"
                            }}
                        />
                    </Stack.Navigator>
                </NavigationContainer>
            </Provider>
        )
    }
    // state = {
    //     catUrl: ''
    // }
    // getCat() {
    //     return fetch('https://aws.random.cat/meow')
    //     .then((response) => response.json())
    //     .then((json) => {
    //         this.setState({catUrl: json.file});
    //     })
    // }
    // componentDidMount() {
    //     this.getCat();
    // }
    // render() {
    //     return(
    //         <View style={styles.container}>
    //             <Image
    //                 source={{uri: this.state.catUrl}}
    //                 style={{width: 200, height: 200}}
    //                 resizeMode='contain'
    //             />
    //         </View>     
    //     )    
    // }
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });

export default App;